/*
	Gitbash: Dependencies
	npm init -y
	npm install express
	npm install mongoose
	npm install cors
	npm install jsonwebtoken
*/



const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

const userRoutes = require("./routes/userRoutes.js")
// from s39
const courseRoutes = require("./routes/courseRoutes.js")

// to create an express server/application 
const app = express();


//Middlewares - it allows to bridge our backend application(server) to our front end

// to allow cross origin resource sharing
app.use(cors());


// to read JSON objects (especially request that is in form of json)
app.use(express.json());


// to read forms
app.use(express.urlencoded({extended: true}));

app.use("/users", userRoutes);
//from s39
app.use("/courses", courseRoutes);

// Connect to our MongoDB Database
mongoose.connect("mongodb+srv://admin:admin@batch230.rc36bvq.mongodb.net/courseBooking?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

mongoose.connection.once("open", () => console.log("Now connected to Aristorenas-Mongo DB Atlas"));



app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT || 4000}`)
});


