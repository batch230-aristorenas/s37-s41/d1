const express = require("express");
const router = express.Router();
const auth = require("../auth.js");
const userControllers = require("../controllers/userControllers.js");

router.post("/register", (request, response) => {
	userControllers.registerUser(request.body).then(resultFromController => response.send(resultFromController))
})

//S38
router.post("/checkEmail", (request, response) => {
	userControllers.checkEmailExist(request.body).then(resultFromController => response.send(resultFromController))
})

router.post("/login", (request, response) => {
	userControllers.loginUser(request.body).then(resultFromController => response.send(resultFromController))
})

//S38 Activity retrieve the profile using _id only in the postman
// router.post("/details", (request, response) => {
// 	userControllers.getProfile(request.body).then(resultFromController => response.send(resultFromController))
// })
/*
 SOLUTION
  SAME
*/


//for Activity s39
router.get("/allUsers", (request, response) => {
	userControllers.getAllUsers(request.body).then(resultFromController => response.send(resultFromController))
})


// s41
// GET method because we will not use reqBody, only the token
router.get("/details", auth.verify, userControllers.getProfile);

router.post("/enroll", auth.verify, userControllers.enroll);

module.exports = router;