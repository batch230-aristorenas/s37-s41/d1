// S39

/*Dependencies*/

const express = require("express");
const router = express.Router();
const courseControllers = require("../controllers/courseControllers.js");
const auth = require("../auth.js");


/*
S39 ACTIVITY
1. Refractor the "course" route to implement user authentication for the admin when creating a course.
*/

// Creating a course
router.post("/create", (request, response) => {
	const newDocs = {
		course: request.body, 	
				//request.headers.authorization contains jwt
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}
	courseControllers.addCourse(request.body, newDocs).then(resultFromController => response.send(resultFromController))
})

// Router for get all courses
router.get("/all", (request, response) => {
	courseControllers.getAllCourses().then(resultFromController => response.send(resultFromController))
})

// Get All ACTIVE
router.get("/active", (request, response) => {
	courseControllers.getActiveCourses().then(resultFromController => response.send(resultFromController))
})


//Get SPECIFIC Course
// nor "reqBody" coz the ID will be typed in the URL itself
// GET method coz no need to type in the body
router.get("/:courseId", (request, response) => {
	courseControllers.getCourse(request.params.courseId).then(resultFromController => response.send(resultFromController))
})

// UPDATE

router.patch("/:courseId/update", auth.verify, (request,response) => {
	const newData = {
		course: request.body, 	
				//request.headers.authorization contains jwt
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	courseControllers.updateCourse(request.params.courseId, newData).then(resultFromController => {
		response.send(resultFromController)
	})
})

// S40 Activity - Updating for Archiving

router.patch("/:courseId/archive", (request, response) => {
	const newArchivedCourse = {
		course: request.body, 	
				//request.headers.authorization contains jwt
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}
	courseControllers.archiveCourse(request.params.courseId, newArchivedCourse).then(resultFromController => response.send(resultFromController))
})

// without Admin verification
// router.patch("/:courseId/archive", (request, response) => {
// 	courseControllers.getCourse(request.params.courseId).then(resultFromController => response.send(resultFromController))
// })


module.exports = router;