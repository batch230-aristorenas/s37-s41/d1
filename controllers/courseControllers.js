// s39
/*Dependencies*/

const mongoose = require("mongoose");
const Course = require("../models/course.js");


// Function for ADDING A COURSE
// 2.Update the "addCourse" controller method to implement admin authentication for creating a course
// NOTE: [1] include screenshot of successful admin addCourse and [2] screenshot of not successful add course by a user that is not admin
/*module.exports.addCourse = (reqBody, newData) => {
	
		let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	return newCourse.save().then((newCourse, error) => {
		if(error){
			return error;
		}
		else{
			return newCourse;
		}
	})
	}*/
	

module.exports.addCourse = (reqBody, newDocs) => {
	 if(newDocs.isAdmin == true){
		let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		slots: reqBody.slots
	})

	return newCourse.save().then((newCourse, error) => {
		if(error){
			return error;
		}
		else{
			return newCourse;
		}
	})
	}
	else{
		let message = Promise.resolve("User must be Admin to access this functionality");
		return message.then((value) => {return value});
	}	
	
}

// Get ALL Course
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
}

//Get All ACTIVE courses
module.exports.getActiveCourses = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	})
}


// Get SPECIFIC course
module.exports.getCourse = (courseId) => {
	return Course.findById(courseId).then(result => {
		return result;
	})
}


// UPDATING a course
module.exports.updateCourse = (courseId, newData) => {
	if(newData.isAdmin == true){
	return Course.findByIdAndUpdate(courseId, 
			{
				name: newData.course.name,
				description: newData.course.description,
				price: newData.course.price
			}
		).then((result, error) => {
			if(error){
				return false;
			}
			return true
		
		})
	}
	else{
		let message = Promise.resolve("User must be Admin to access this functionality");
		return message.then((value) => {return value});
	}	
}

// S40 Activity - Updating for Archiving

module.exports.archiveCourse = (courseId, newArchivedCourse) => {
	if(newArchivedCourse.isAdmin == true){
		return Course.findByIdAndUpdate(courseId,
			{
				isActive: newArchivedCourse.course.isActive
			}
		).then((result, error) => {
			if(error){
				return false;
			}
			return true
		})
	}
	else{
		let message = Promise.resolve("User must be Admin to access this functionality");
		return message.then((value) => {return value});
	}	
}
// without Admin verification
// module.exports.archiveCourse = (reqBody) => {
// 	return Course.findByIdAndUpdate(reqBody.isActive, {
// 		isActive: reqBody.isAdmin
// 	}).then(result => {
// 		return result;
// 	})
// }